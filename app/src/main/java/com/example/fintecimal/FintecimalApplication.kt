package com.example.fintecimal

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FintecimalApplication : Application()
package com.example.fintecimal.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.fintecimal.R
import com.example.fintecimal.databinding.ListItemPlacesBinding
import com.example.fintecimal.pojos.Place
import com.example.fintecimal.presenters.contracts.IHomePresenter

class PlacesAdapter(
    context: Context,
    private val list: List<Place>,
    var presenter: IHomePresenter
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = inflater.inflate(R.layout.list_item_places, parent, false)
        return ItemViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val row = list[position]
        (holder as ItemViewHolder).refresh(row)
    }

    private inner class ItemViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {
        private val binding: ListItemPlacesBinding = DataBindingUtil.bind(view)!!

        init {
            binding.presenter = presenter
        }

        fun refresh(item: Place) {
            binding.item = item
            binding.executePendingBindings()
        }
    }

}
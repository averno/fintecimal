package com.example.fintecimal.di.environment

import android.app.Application
import com.example.fintecimal.R
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Named

@Module
@InstallIn(SingletonComponent::class)
object EnvironmentModule {

    @Provides
    @Named("apiUrl")
    fun provideApiUrl(
        application: Application
    ): String = application.resources.getString(R.string.pref_api_url_key)

}
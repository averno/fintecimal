package com.example.fintecimal.di.fragment

import com.example.fintecimal.presenters.HomePresenter
import com.example.fintecimal.presenters.contracts.IHomePresenter
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.scopes.FragmentScoped

@Module
@InstallIn(FragmentComponent::class)
abstract class HomeFragmentModule {

    @Binds
    @FragmentScoped
    abstract fun bindHomePresenter(presenter: HomePresenter): IHomePresenter
}
package com.example.fintecimal.di.fragment

import com.example.fintecimal.presenters.MapPresenter
import com.example.fintecimal.presenters.contracts.IMapPresenter
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.scopes.FragmentScoped

@Module
@InstallIn(FragmentComponent::class)
abstract class MapFragmentModule {

    @Binds
    @FragmentScoped
    abstract fun bindMapPresenter(presenter: MapPresenter): IMapPresenter
}
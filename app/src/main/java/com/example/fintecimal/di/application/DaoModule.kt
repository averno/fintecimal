package com.example.fintecimal.di.application

import android.app.Application
import androidx.room.Room
import com.example.fintecimal.dao.RoomDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object DaoModule {

    @Singleton
    @Provides
    fun provideDatabase(application: Application) = Room
        .databaseBuilder(application, RoomDatabase::class.java, RoomDatabase.DB_NAME)
        .allowMainThreadQueries()
        .fallbackToDestructiveMigration()
        .build()

    @Singleton
    @Provides
    fun provideplaceDao(database: RoomDatabase) = database.placeDAO()

}
package com.example.fintecimal.presenters.contracts

import com.example.fintecimal.pojos.Place

interface IMapPresenter: IPresenter<IMapPresenter.View> {

    fun onNavigateClick()

    fun onVisitClick()

    fun onResume(place: Place)

    fun onMapReady()



    interface View : IPresenter.IView {
        /**
         * Updates the Map with the pending place to visit
         */
        fun updateMap(place: Place)

        /**
         * Navigates to Map Chooser
         */
        fun navigate(place: Place)

        /**
         * Navigates to List Fragment
         */
        fun goBack()


    }
}
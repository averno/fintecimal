package com.example.fintecimal.presenters.contracts

interface IPresenter<T : IPresenter.IView> {

    /**
     * Attach _view to presenter
     * @param view related _view instance
     */
    fun attachView(view: T)

    /**
     * Fired at the end of fragment onCreateView
     * Notify if needed
     */
    fun onCreateView()

    /**
     * Fired at the end of activity onCreate
     * Notify if needed
     */
    fun onCreated()

    /**
     * Fired at the start of activity onStart
     * Notify if needed
     */
    fun onStart()

    /**
     * Fired at the start of activity onResume
     * Notify if needed
     */
    fun onResume()

    /**
     * Fired at the start of activity onPause
     * Notify if needed
     */
    fun onPause()

    /**
     * Fired at the start of activity onStop
     * Notify if needed
     */
    fun onStop()

    /**
     * Fired at the start of activity onDestroy
     * Important: Notify always
     */
    fun onDestroy()

    interface IView
}
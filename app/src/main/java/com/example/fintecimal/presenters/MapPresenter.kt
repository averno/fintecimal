package com.example.fintecimal.presenters

import com.example.fintecimal.pojos.Place
import com.example.fintecimal.presenters.contracts.IMapPresenter
import com.example.fintecimal.repository.IHomeRepository
import javax.inject.Inject

class MapPresenter @Inject constructor(
    private val homeRepository: IHomeRepository
): Presenter<IMapPresenter.View>(), IMapPresenter {

    private lateinit var place: Place

    override fun onResume(place: Place) {
        super.onResume()
        this.place = place
    }

    override fun onMapReady() {
        view.updateMap(place)
    }

    override fun onNavigateClick(){
        view.navigate(place)
    }

    override fun onVisitClick() {
        homeRepository.setPlaceVisited(place)
        view.goBack()
    }

}
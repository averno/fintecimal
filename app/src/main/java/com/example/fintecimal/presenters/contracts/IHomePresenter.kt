package com.example.fintecimal.presenters.contracts

import com.example.fintecimal.pojos.Place

interface IHomePresenter: IPresenter<IHomePresenter.View> {

    fun onItemSelected(streetName: String)

    interface View : IPresenter.IView {

        /**
         * Show or hide loading indicator
         * @param show
         */
        fun showLoading(show: Boolean)

        fun showPlaces(places: List<Place>, pendingPlaces: Int)

        /**
         * Show error message
         * @param message
         */
        fun showError(message: String)

        fun navigateToMap(place: Place)


    }
}
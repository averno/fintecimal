package com.example.fintecimal.presenters

import com.example.fintecimal.pojos.Place
import com.example.fintecimal.presenters.contracts.IHomePresenter
import com.example.fintecimal.repository.IHomeRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomePresenter @Inject constructor (
    private val homeRepository: IHomeRepository
) : Presenter<IHomePresenter.View>(), IHomePresenter {

    private var places = emptyList<Place>()

    override fun onResume() {
        super.onResume()
        findPlaces()
    }

    private fun findPlaces() = launch{
        view.showLoading(true)
        try{
            places = homeRepository.getPlaces()
            _view?.showPlaces(places, places.count { !it.visited })

        } catch(e:Exception) {
            // if error on repository  catch this exception on show to presentable message
            _view?.showError(e.toString())
        } finally{
            _view?.showLoading(false)
        }
    }

    override fun onItemSelected(streetName: String) {
        val a = places.find { it.streetName == streetName }
        view.navigateToMap(a!!)

    }

}
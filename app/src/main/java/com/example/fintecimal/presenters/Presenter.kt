package com.example.fintecimal.presenters

import androidx.annotation.CallSuper
import com.example.fintecimal.presenters.contracts.IPresenter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext


abstract class Presenter<T : IPresenter.IView> : IPresenter<T>, CoroutineScope {


    /**
     * Coroutine Scope is defined
     */
    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    /**
     * Null safe view
     */
    protected var _view: T? = null

    /**
     * Use this getter property only when you already know that view is not null
     */
    protected val view get() = _view!!

    @CallSuper
    override fun attachView(view: T) {
        this._view = view
    }

    override fun onCreateView() {}

    override fun onCreated() {}

    override fun onResume() {}

    override fun onPause() {}

    override fun onStop() {}

    override fun onStart() {}

    @CallSuper
    override fun onDestroy() {
        // detach _view from presenter
        _view = null
    }
}
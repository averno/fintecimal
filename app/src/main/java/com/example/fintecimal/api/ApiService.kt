package com.example.fintecimal.api

import com.example.fintecimal.pojos.Place
import retrofit2.Call
import retrofit2.http.GET


interface ApiService {

    @GET("interview")
    fun getPlaces(): Call<List<Place>>

}
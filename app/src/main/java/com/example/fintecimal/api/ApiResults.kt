package com.example.fintecimal.api

import retrofit2.Call


object ApiResults {

    /**
     * Generic function for requests without ApiBody as response body, reduces boilerplate id.
     * @param call the call for API
     * @return
     */
    fun <T> createForNonApiBody(call: Call<T>): T {
        val response = call.execute()

        if (!response.isSuccessful) {
            throw Exception(response.message() + " (code: ${response.code()})")
        }

        // success
        return response.body() ?: throw Exception("Response is null")
    }
}


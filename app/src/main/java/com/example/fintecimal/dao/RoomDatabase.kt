package com.example.fintecimal.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.fintecimal.pojos.LocationConverter
import com.example.fintecimal.pojos.Place

@Database(
    entities = [
        Place::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(LocationConverter::class)
abstract class RoomDatabase : RoomDatabase() {

    abstract fun placeDAO(): PlaceDao

    companion object {

        const val DB_NAME = "fintec-8754u.db"
    }
}
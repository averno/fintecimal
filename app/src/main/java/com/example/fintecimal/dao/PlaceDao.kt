package com.example.fintecimal.dao

import androidx.room.*
import com.example.fintecimal.pojos.Place


@Dao
interface PlaceDao {

    /**
     * Retrieve list of places
     * @return List<Place>
     */
    @Query("select * from place")
    fun findAll(): List<Place>

    /**
     * Save place in DB
     * @param place
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(place: Place)

    /**
     * CSave place list in DB
     * @param places
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(places: List<Place>)

    /**
     * update the place in db and set it to visited
     * @param place
     */
    @Update
    fun setVisited(place: Place)

    /**
     * Delete the list from db
     */
    @Query("delete from place")
    fun deleteAll()
}
package com.example.fintecimal.pojos

import androidx.room.TypeConverter
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson

class LocationConverter {

        @TypeConverter
        fun toLocation(locationString: String?) = try{
                Gson().fromJson(locationString, LatLng::class.java)
        } catch(e: Exception){
                null
        }

        @TypeConverter
        fun toLocationString(location: LatLng?): String?{
                return Gson().toJson(location)
        }

}
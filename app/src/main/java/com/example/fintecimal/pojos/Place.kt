package com.example.fintecimal.pojos

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "place")
data class Place (
        @PrimaryKey(autoGenerate = true)
        var id: Long,
        var streetName: String,
        var suburb: String,
        var visited: Boolean,
        var location: LatLng
): Parcelable
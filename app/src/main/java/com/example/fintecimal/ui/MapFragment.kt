package com.example.fintecimal.ui

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.fintecimal.BaseFragment
import com.example.fintecimal.R
import com.example.fintecimal.databinding.FragmentMapBinding
import com.example.fintecimal.pojos.Place
import com.example.fintecimal.presenters.contracts.IMapPresenter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MapFragment : BaseFragment<FragmentMapBinding>(R.layout.fragment_map), IMapPresenter.View {

    private val args by navArgs<MapFragmentArgs>()
    private lateinit var googleMap: GoogleMap

    @Inject
    lateinit var presenter: IMapPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attachView(this)
        (activity as AppCompatActivity).supportActionBar!!.show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync {
            googleMap = it
            presenter.onMapReady()
        }

        binding.presenter = presenter
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume(args.currentPlace)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun updateMap(place: Place) {
        binding.item = place
        googleMap.addMarker(
            MarkerOptions()
                .position(place.location)
                .icon(
                    bitmapDescriptorFromVector(
                        requireContext(),
                        if (place.visited) R.drawable.ic_visited_marker else R.drawable.ic_pending_marker
                    )
                )
        )
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(place.location))
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(14.0f))
    }

    override fun navigate(place: Place) {
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse(
                "geo:${place.location.latitude},${place.location.longitude}?q=${place.streetName}"//?z=14"
            )
        )

        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(requireContext(), "Debes instalar una app de mapas.", Toast.LENGTH_LONG)
                .show()
        }
    }

    override fun goBack() {
        val backAction = MapFragmentDirections.goBackAction()
        findNavController().navigate(backAction)
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        return ContextCompat.getDrawable(context, vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap =
                Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }
}
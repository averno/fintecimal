package com.example.fintecimal.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.example.fintecimal.BaseFragment
import com.example.fintecimal.R
import com.example.fintecimal.adapters.PlacesAdapter
import com.example.fintecimal.databinding.FragmentHomeBinding
import com.example.fintecimal.pojos.Place
import com.example.fintecimal.presenters.contracts.IHomePresenter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(R.layout.fragment_home) , IHomePresenter.View{

    @Inject
    lateinit var presenter: IHomePresenter

    private var places = mutableListOf<Place>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attachView(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.presenter = presenter
        setUpRecyclerView()
    }

    private fun setUpRecyclerView() {
        binding.rvPlaces.adapter = PlacesAdapter(requireContext(), places, presenter)
    }

    override fun showPlaces(places: List<Place>, pendingPlaces: Int) {
        this.places.clear()

        binding.pendingplaces = pendingPlaces != 0

        if(pendingPlaces != 0){
            this.places.addAll(places)
            binding.tvPendingVisits.text = getString(R.string.first_pending_visits, pendingPlaces)
        }

        binding.rvPlaces.adapter?.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }


    override fun showLoading(show: Boolean) {
        binding.loading = show
    }

    override fun showError(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }

    override fun navigateToMap(place: Place) {
        val mapAction = HomeFragmentDirections.mapAction(place)
        findNavController().navigate(mapAction)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

}
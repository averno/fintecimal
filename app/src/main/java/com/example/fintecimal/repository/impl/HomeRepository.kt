package com.example.fintecimal.repository.impl

import android.content.Context
import android.content.SharedPreferences
import com.example.fintecimal.R
import com.example.fintecimal.api.ApiResults
import com.example.fintecimal.api.ApiService
import com.example.fintecimal.dao.PlaceDao
import com.example.fintecimal.pojos.Place
import com.example.fintecimal.repository.IHomeRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class HomeRepository @Inject constructor(
    private val placeDao: PlaceDao,
    private val preferences: SharedPreferences,
    context: Context,
    private val api: ApiService
) : IHomeRepository {

    private val firstLaunchKey = context.getString(R.string.pref_first_launch_key)

    override suspend fun getPlaces() : List<Place> {

        val firstLaunch = preferences.getBoolean(
            firstLaunchKey,
            true
        )

        if(firstLaunch){
            var places = listOf<Place>()
            withContext(Dispatchers.IO){
                places = ApiResults.createForNonApiBody(api.getPlaces())
            }
            placeDao.insertAll(places)

            // save preference change
            val editor = preferences.edit()
            editor.putBoolean(firstLaunchKey, false)
            editor.apply()
        }

        return placeDao.findAll()
    }

    override fun setPlaceVisited(place: Place) {
        place.visited = true
        placeDao.setVisited(place)
    }
}
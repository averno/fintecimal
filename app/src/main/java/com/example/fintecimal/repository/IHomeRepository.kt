package com.example.fintecimal.repository

import com.example.fintecimal.pojos.Place

interface IHomeRepository {

    suspend fun getPlaces() : List<Place>

    fun setPlaceVisited(place: Place)
}